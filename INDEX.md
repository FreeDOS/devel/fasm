# flat assembler, version 1.73.32

The flat assembler (abbreviated to fasm) is a fast self-assembling assembler for DOS, Windows and Linux operating systems. It was designed primarily for the assembly of x86 instructions and it supports x86 and x86-64 instructions sets with MMX, 3DNow!, SSE up to SSE4, AVX, AVX2, XOP and AVX-512 extensions and can produce output in plain binary, MZ, PE, COFF or ELF format. It includes the powerful but easy to use macroinstruction support and does multiple passes to optimize the size of instruction codes. The flat assembler is self-hosting and the complete source code is included.

# flat assembler

The repository for this project is only a duplicate. It is actively
maintained by it's developer and a more recent version may exist at the
original developers website.

If you wish to contribute or submit any bug reports for this project, please
visit the developers website at https://flatassembler.net

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

# FASM.LSM

<table>
<tr><td>title</td><td>flat assembler</td></tr>
<tr><td>version</td><td>1.73.32</td></tr>
<tr><td>entered&nbsp;date</td><td>2024-03-31</td></tr>
<tr><td>description</td><td>Fast but powerful 80x86 assembler</td></tr>
<tr><td>summary</td><td>The flat assembler (abbreviated to fasm) is a fast self-assembling assembler for DOS, Windows and Linux operating systems. It was designed primarily for the assembly of x86 instructions and it supports x86 and x86-64 instructions sets with MMX, 3DNow!, SSE up to SSE4, AVX, AVX2, XOP and AVX-512 extensions and can produce output in plain binary, MZ, PE, COFF or ELF format. It includes the powerful but easy to use macroinstruction support and does multiple passes to optimize the size of instruction codes. The flat assembler is self-hosting and the complete source code is included.</td></tr>
<tr><td>keywords</td><td>assembler, asm, fasm</td></tr>
<tr><td>author</td><td>Tomasz Grysztar <privalov _AT_ o2.pl></td></tr>
<tr><td>maintained&nbsp;by</td><td>Tomasz Grysztar <privalov _AT_ o2.pl></td></tr>
<tr><td>primary&nbsp;site</td><td>[http://flatassembler.net/](http://flatassembler.net/)</td></tr>
<tr><td>original&nbsp;site</td><td>[http://fasm.sourceforge.net/](http://fasm.sourceforge.net/)</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Modified (2-Clause) BSD License](LICENSE)</td></tr>
</table>
